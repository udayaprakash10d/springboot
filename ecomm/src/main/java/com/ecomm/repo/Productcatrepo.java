package com.ecomm.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ecomm.model.ProductCat;
@Repository
public interface Productcatrepo extends JpaRepository<ProductCat, Integer> {

}
