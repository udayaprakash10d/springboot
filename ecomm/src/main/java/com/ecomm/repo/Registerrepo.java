package com.ecomm.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import com.ecomm.model.Register;

@Repository
public interface Registerrepo extends JpaRepository<Register, Integer> {

	@Query("SELECT t FROM Register t where t.emailId = :emailId")
	public Register getByEmailId(@Param("emailId") String emailId);

	@Query("SELECT t FROM Register t where t.emailId = :emailId And t.password=:password")
	public Register getByEmailIdAndPassWord(String emailId, String password);

}
