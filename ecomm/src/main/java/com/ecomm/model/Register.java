package com.ecomm.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(uniqueConstraints={@UniqueConstraint(columnNames={"emailId"})})
public class Register {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int id;
	public String userName;
	public String password;
	public long mobNo;
	public String emailId;

	@OneToMany(targetEntity = Cart.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "Register_id", referencedColumnName = "id")
     public List<Cart> cart;




	public List<Cart> getCart() {
		return cart;
	}

	public void setCart(List<Cart> cart) {
		this.cart = cart;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public long getMobNo() {
		return mobNo;
	}

	public void setMobNo(long mobNo) {
		this.mobNo = mobNo;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Register() {
		super();
	
	}

	public Register(int id, String userName, String password, long mobNo, String emailId, List<Cart> cart) {
		super();
		this.id = id;
		this.userName = userName;
		this.password = password;
		this.mobNo = mobNo;
		this.emailId = emailId;
		this.cart =  cart;
	}

	@Override
	public String toString() {
		return "Register [id=" + id + ", userName=" + userName + ", password=" + password + ", mobNo=" + mobNo
				+ ", emailId=" + emailId + ", cart=" + cart + "]";
	}

	


	
      
}
	



