package com.ecomm.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ecomm.model.ProductCat;
import com.ecomm.repo.Productcatrepo;

@Component
public class Productcatservice {

	@Autowired
	public Productcatrepo productcatrepo;

	public ProductCat saveCategory(ProductCat productcat) {

		return productcatrepo.save(productcat);
	}

	public Optional<ProductCat> getCategoryByID(int categoryid) {

		return productcatrepo.findById(categoryid);
	}

	public ProductCat update(ProductCat productcat) {

		return productcatrepo.save(productcat);
	}

}
