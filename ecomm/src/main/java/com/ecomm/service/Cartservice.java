package com.ecomm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ecomm.model.Cart;
import com.ecomm.repo.Cartrepo;

@Component
public class Cartservice {

	@Autowired
	public Cartrepo cartrepo;

	public Cart save(Cart cart) {

		return cartrepo.save(cart);
	}

	public Cart getCartId(int cartid) {

		return cartrepo.findById(cartid).get();
	}

	public Cart updateCart(Cart cart) {

		return cartrepo.save(cart);
	}

}
