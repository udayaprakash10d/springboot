package com.ecomm.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.ecomm.model.Product;
import com.ecomm.repo.Productrepo;

@Component
public class Productservice {

	@Autowired
	public Productrepo productlistrepo;

	public Product saveProducts(Product productlist) {

		return productlistrepo.save(productlist);
	}

	public Optional<Product> getPrductListById(int productid) {

		return productlistrepo.findById(productid);
	}

	public Product updateProductList(Product productlist) {

		return productlistrepo.save(productlist);
	}

}
