package com.ecomm.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ecomm.model.Register;
import com.ecomm.repo.Registerrepo;

@Component
public class Registerservice {

	@Autowired
	public Registerrepo registerrepo;

	public Register addRegister(Register register) {

		return registerrepo.save(register);
	}

	public Optional<Register> getUserByID(int id) {

		return registerrepo.findById(id);
	}

	public Register updateUser(Register register) {

		return registerrepo.save(register);
	}

	public void deleteUser(int id) {
		registerrepo.deleteById(id);

	}

	public Register getByEmailId(String emailId) {

		return registerrepo.getByEmailId(emailId);
	}

}
