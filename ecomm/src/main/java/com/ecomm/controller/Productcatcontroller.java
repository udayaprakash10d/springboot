package com.ecomm.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ecomm.model.ProductCat;
import com.ecomm.service.Productcatservice;

@RestController
public class Productcatcontroller {
	@Autowired
	public Productcatservice productcatservice;

	@PostMapping("/category")
	public ProductCat saveCategory(@RequestBody ProductCat productcat) {
		return productcatservice.saveCategory(productcat);

	}

	@PutMapping("/category")
	public ProductCat update(@RequestBody ProductCat productcat) {
		return productcatservice.update(productcat);
	}

	@GetMapping("/category/{categoryid}")
	public Optional<ProductCat> getCategoryByID(@PathVariable("categoryid") int categoryid) {
		return productcatservice.getCategoryByID(categoryid);
	}

}
