package com.ecomm.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ecomm.model.Cart;
import com.ecomm.model.Product;
import com.ecomm.model.Register;
import com.ecomm.repo.Productrepo;
import com.ecomm.repo.Registerrepo;
import com.ecomm.service.Cartservice;

@RestController
public class Cartcontroller {

	@Autowired
	public Cartservice cartservice;

	@Autowired
	public Registerrepo registerrepo;

	@Autowired
	public Productrepo productlistrepo;

	@PostMapping("/cart/{id}/{productid}")
	public Cart save(@RequestBody Cart cart, @PathVariable(value = "id") int id,
			@PathVariable(value = "productid") int productid) {

		Product productlist = productlistrepo.findById(productid).get();

		Register register = registerrepo.findById(id).get();

		if ((register != null) && (productlist != null)) {

			cart.setProductlist(productlist);
			cart.setRegister(register);
		}
		if (cart.getStatus() == 1) {

			productlist.setCountItem(productlist.getCountItem() - cart.getCount());
			productlistrepo.save(productlist);

		}

		return cartservice.save(cart);
	}

	@GetMapping("/cart/{cartid}")
	public Cart getCartId(@PathVariable(value = "cartid") int cartid) {
		return cartservice.getCartId(cartid);
	}

	@PutMapping("/cart")
	public Cart updateCart(@RequestBody Cart cart) {
		return cartservice.updateCart(cart);

	}

}
