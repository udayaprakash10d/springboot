package com.ecomm.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ecomm.model.Register;
import com.ecomm.repo.Registerrepo;
import com.ecomm.service.Registerservice;

@RestController
public class Registercontroller {

	@Autowired
	public Registerrepo registerrepo;

	@Autowired
	public Registerservice registerservice;

	@PostMapping("/user")
	public Register addRegister(@RequestBody Register register) {
		return registerservice.addRegister(register);
	}

	@GetMapping("/user/{id}")
	public Optional<Register> getUsersByID(@PathVariable("id") int id) {
		return registerservice.getUserByID(id);

	}

	@PutMapping("/user/{id}")
	public Register updateUser(@RequestBody Register register, @PathVariable("id") int id) {
		Register register1 = registerservice.getUserByID(id).get();
		System.out.println(register1);
		if (register1 != null) {
			register1.setMobNo(register.getMobNo());
			register1.setEmailId(register.getEmailId());
			register1.setPassword(register.getPassword());
			register1.setUserName(register.getUserName());
		}
		return registerservice.updateUser(register1);

	}

	@DeleteMapping("/user/{id}")
	public void deleteUser(@PathVariable("id") int id) {
		registerservice.deleteUser(id);
	}

	@GetMapping("/user1/{emailId}")
	public Register getByEmailId(@PathVariable("emailId") String emailId) {
		return registerservice.getByEmailId(emailId);
	}

	@PostMapping("/login")
	public String login(@RequestBody Register register) {

		Register register2 = registerrepo.getByEmailIdAndPassWord(register.getEmailId(), register.getPassword());
		System.out.println(register2);
		if (register2 != null) {
			return "sucess";

		} else {
			return "invalid ";
		}

	}

}
