package com.ecomm.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ecomm.model.ProductCat;
import com.ecomm.model.Product;
import com.ecomm.repo.Productcatrepo;
import com.ecomm.service.Productservice;

@RestController
public class Productcontroller {

	@Autowired
	public Productservice productlistservice;

	@Autowired
	public Productcatrepo productcatrepo;

	@PostMapping("/product/{categoryid}")
	public Product saveProducts(@RequestBody Product productlist, @PathVariable(value = "categoryid") int categoryid) {
		ProductCat productcat = productcatrepo.findById(categoryid).get();
		if (productcat != null) {
			productlist.setProductcat(productcat);
		}
		return productlistservice.saveProducts(productlist);
	}

	@GetMapping("/product/{productid}")
	public Optional<Product> getPrductListById(@PathVariable("productid") int productid) {
		return productlistservice.getPrductListById(productid);
	}

	@PutMapping("product")
	public Product updateProductList(@RequestBody Product productlist) {
		return productlistservice.updateProductList(productlist);
	}

}
