package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Model;
import com.example.service.Service;
@RestController
public class Controller {
	
	@Autowired
	public Service service;
	
	@PostMapping("/post")
	public Model post(@RequestBody Model model)
	{
		return service.post(model);
	}
	
	@GetMapping("/get")
	public List<Model> get ()
	{
		return service.get();
	}

}
