package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.model.Model;
import com.example.repo.Repo;
@Component
public class Service {
	@Autowired
	public Repo repo;

	public Model post(Model model) {
	
		return repo.save(model);
	}

	public List<Model> get() {
		
		return repo.findAll();
	}

}
