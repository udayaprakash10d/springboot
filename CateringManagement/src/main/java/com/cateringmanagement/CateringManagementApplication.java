package com.cateringmanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CateringManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(CateringManagementApplication.class, args);
	}

}
