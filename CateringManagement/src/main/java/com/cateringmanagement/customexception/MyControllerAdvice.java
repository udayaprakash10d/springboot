package com.cateringmanagement.customexception;

import java.util.NoSuchElementException;

import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;



@ControllerAdvice
public class MyControllerAdvice extends ResponseEntityExceptionHandler{
	Logger logger = LoggerFactory.getLogger(MyControllerAdvice.class);
	
	@ExceptionHandler(NoSuchElementException.class)
	public ResponseEntity<String> handleNoSuchElementException(NoSuchElementException elementException)
	{
		logger.error("invalid id");
		return new ResponseEntity<String>("no value present in DB",HttpStatus.NOT_FOUND);
	}
	
    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex,
    		HttpHeaders headers, HttpStatus status, WebRequest request) {
    	logger.error("invalid request"+ex.getMessage());
    	return new ResponseEntity<Object>("please change ur request",HttpStatus.NOT_FOUND);
    	
    }
    @ExceptionHandler(EmptyResultDataAccessException.class)
    public ResponseEntity<String> emptyResultDataAccessException(EmptyResultDataAccessException  e)
	{
		logger.error("invalid id"+e.getMessage());
		return new ResponseEntity<String>("no value present in DB",HttpStatus.NOT_FOUND);
	}
    @Override
          protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
    		HttpHeaders headers, HttpStatus status, WebRequest request) {
    	   logger.error("invalid argument"+ex.getMessage());
    	   return new ResponseEntity<Object>("arguments not valid",HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<String>constraintViolation (ConstraintViolationException  e)
	{
		logger.error("exception in constraint violation"+e.getMessage());
		return new ResponseEntity<String>("ConstraintViolationException",HttpStatus.BAD_REQUEST);
	}
	
//    @ExceptionHandler(Exception.class)
//    public ResponseEntity<String> handleAnyException(Exception  e)
//	{
//		logger.error("invalid id"+e.getMessage());
//		return new ResponseEntity<String>("exception happend",HttpStatus.NOT_FOUND);
//	}
     
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
    		HttpHeaders headers, HttpStatus status, WebRequest request) {
    	logger.error("request has empty body"+ex.getMessage());
    	return new ResponseEntity<Object>("request has empty body ",HttpStatus.BAD_REQUEST);
    }
    
   

 
    
    
   
	

}
