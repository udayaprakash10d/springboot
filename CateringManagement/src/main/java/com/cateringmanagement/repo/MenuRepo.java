package com.cateringmanagement.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cateringmanagement.model.Menu;

@Repository
public interface MenuRepo extends JpaRepository<Menu, Integer> {

}
