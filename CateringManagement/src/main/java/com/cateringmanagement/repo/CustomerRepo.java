package com.cateringmanagement.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cateringmanagement.model.Customer;

@Repository
public interface CustomerRepo extends JpaRepository<Customer, Integer> {

	Customer findByUserName(String userName);

//	Customer findByUsername(String userName);

	//Customer getByUserNameAndPassword(String userName, String password);

	//Customer findByUsername(String username);

	
}
