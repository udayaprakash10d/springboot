package com.cateringmanagement.controller;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cateringmanagement.model.Customer;
import com.cateringmanagement.repo.CustomerRepo;
import com.cateringmanagement.service.CustomerService;
//import com.cateringmanagement.utility.GlobalResource;

@RestController
public class CustomerController {
	@Autowired
	CustomerService customerService;
	@Autowired
	CustomerRepo customerRepo;

	Logger logger = LoggerFactory.getLogger(CustomerController.class);

	@PostMapping("/customer")
	public ResponseEntity<Customer> addcustomer(@RequestBody Customer customer) {
		String methodName = "addCustomer";
		logger.info(methodName + "called");
		Customer customersave = customerService.addCustomer(customer);
		return new ResponseEntity<Customer>(customersave, HttpStatus.CREATED);
	}

	@GetMapping("/customer/{id}")
	public ResponseEntity<Customer> getCustomer(@PathVariable("id") int id) {
		String methodName = "getCustomer";
		logger.info(methodName + "called");
		Customer customer = customerService.getCustomer(id);
		return new ResponseEntity<Customer>(customer, HttpStatus.OK);

	}

	@PutMapping("/customer/{id}")
	public ResponseEntity<Customer> updateCustomer(@RequestBody Customer customer, @PathVariable("id") int id) {
		String methodName = "updateCustomer";
		logger.info(methodName + "called");
		Customer customer1 = customerService.getCustomer(id);
		if (customer1 != null) {
			customer1.setFirstName(customer.getFirstName());
			customer1.setLastName(customer.getLastName());
			customer1.setMobileNo(customer.getMobileNo());
			customer1.setPassword(customer.getPassword());
			customer1.setUserName(customer.getUserName());
		}
		customerService.updateCustomer(customer1);
		return new ResponseEntity<Customer>(customer, HttpStatus.CREATED);
	}

	@DeleteMapping("/customer/{id}")
	public ResponseEntity<Void> deleteCustomer(@PathVariable("id") int id) {
		String methodName = "deleteCustomer";
		logger.info(methodName + "called");
		customerService.deleteCustomer(id);
		return new ResponseEntity<Void>(HttpStatus.ACCEPTED);

	}

}
