package com.cateringmanagement.controller;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cateringmanagement.model.FoodItem;
import com.cateringmanagement.model.Menu;
import com.cateringmanagement.service.MenuService;
//import com.cateringmanagement.utility.GlobalResource;

@RestController
public class MenuController {
	@Autowired
	MenuService menuService;

	Logger logger = LoggerFactory.getLogger(MenuController.class);

	@PostMapping("/menu")
	public ResponseEntity<Menu> addMenu(@RequestBody Menu menu) {
		String methodName = "addMenu";
		logger.info(methodName + "called");

		Menu menuitem = menuService.addMenu(menu);

		return new ResponseEntity<Menu>(menuitem, HttpStatus.CREATED);

	}

	@GetMapping("/menu/{id}")
	public ResponseEntity<Menu> getMenu(@PathVariable("id") int id) {
		String methodName = "getMenu";
		logger.info(methodName + "called");

		Menu menu = menuService.getMenu(id).get();

		return new ResponseEntity<Menu>(menu, HttpStatus.OK);
	}

	@PutMapping("/menu")
	public ResponseEntity<Menu> updateMenu(@RequestBody Menu menu) {
		String methodName = "updateMenu";
		logger.info(methodName + "called");

		Menu updatemenu = menuService.updateMenu(menu);

		return new ResponseEntity<Menu>(updatemenu, HttpStatus.CREATED);
	}

	@PutMapping("/menu/{id}")
	public ResponseEntity<Menu> updateMenuById(@RequestBody Menu menu, @PathVariable("id") int id) {
		String methodName = "updateMenuById";
		logger.info(methodName + "called");

		Menu menu1 = menuService.getMenu(id).get();
		if (menu1 != null) {
			menu1.setMenuName(menu.getMenuName());
		}
		menuService.updateMenuById(menu1);

		return new ResponseEntity<Menu>(menu1, HttpStatus.CREATED);
	}

	@DeleteMapping("/menu/{id}")
	public ResponseEntity<Void> deleteMenu(@PathVariable("id") int id) {
		String methodName = "deleteMenu";
		logger.info(methodName + "called");

		menuService.deleteMenu(id);
		return new ResponseEntity<Void>(HttpStatus.ACCEPTED);

	}

}
