package com.cateringmanagement.controller;

import java.util.InputMismatchException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cateringmanagement.model.Cart;
import com.cateringmanagement.model.Customer;
import com.cateringmanagement.model.FoodItem;

import com.cateringmanagement.repo.CustomerRepo;
import com.cateringmanagement.repo.FoodItemRepo;
import com.cateringmanagement.service.CartService;
//import com.cateringmanagement.utility.GlobalResource;

@RestController
public class CartController {
	@Autowired
	CartService cartService;
	@Autowired
	FoodItemRepo foodItemRepo;

	@Autowired
	CustomerRepo customerRepo;

	Logger logger = LoggerFactory.getLogger(CartController.class);

	@PostMapping("/cart/{id}/{foodid}")
	public ResponseEntity<Cart> addCart(@RequestBody Cart cart, @PathVariable("id") int id,
			@PathVariable("foodid") int foodid) {
		String methodName = "addCart";
		logger.info(methodName + "called");

		FoodItem foodItem = foodItemRepo.findById(foodid).get();
		Customer customer = customerRepo.findById(id).get();
		if ((foodItem != null) && (customer != null)) {
			cart.setFoodItem(foodItem);
			cart.setCustomer(customer);
		}
		if (cart.getStatus() == 1) {
			foodItem.setQuantity(foodItem.getQuantity() - cart.getQuantity());
			foodItemRepo.save(foodItem);

		}

		Cart cartSave = cartService.addCart(cart);
		return new ResponseEntity<Cart>(cartSave, HttpStatus.CREATED);
	}

	@GetMapping("/cart/{cartid}")
	public ResponseEntity<Cart> getCartById(@PathVariable(value = "cartid") int cartid) {
		String methodName = "getCartById";
		logger.info(methodName + "called");
		Cart getCart = cartService.getCartById(cartid);
		return new ResponseEntity<Cart>(getCart, HttpStatus.OK);
	}

	@DeleteMapping("/cart/{cartid}")
	public ResponseEntity<Void> deleteCartById(@PathVariable("cartid") int cartid) {
		String methodName = "deleteCartById";
		logger.info(methodName + "called");
		cartService.deleteCartById(cartid);
		// logger.info("customer deleted");
		return new ResponseEntity<Void>(HttpStatus.ACCEPTED);
	}

	@PutMapping("/cart/{cartid}")
	public ResponseEntity<Cart> updateCart(@RequestBody Cart cart, @PathVariable("cartid") int cartid) {
		String methodName = "updateCart";
		logger.info(methodName + "called");

		Cart cart1 = cartService.getCartById(cartid);
		if (cart1 != null) {
			cart1.setStatus(cart.getStatus());
			cart1.setDate(cart.getDate());
			cart1.setQuantity(cart.getQuantity());
		}
		cartService.updateCart(cart1);
		return new ResponseEntity<Cart>(cart1, HttpStatus.CREATED);

	}

}
