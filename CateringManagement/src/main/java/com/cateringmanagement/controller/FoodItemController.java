package com.cateringmanagement.controller;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cateringmanagement.model.Cart;
import com.cateringmanagement.model.Customer;
import com.cateringmanagement.model.FoodItem;
import com.cateringmanagement.model.Menu;
import com.cateringmanagement.repo.MenuRepo;
import com.cateringmanagement.service.FoodItemService;
//import com.cateringmanagement.utility.GlobalResource;

@RestController
public class FoodItemController {
	@Autowired
	FoodItemService foodItemService;
	@Autowired
	MenuRepo menuRepo;

	Logger logger = LoggerFactory.getLogger(FoodItemController.class);

	@PostMapping("/fooditem/{menuid}")
	public ResponseEntity<FoodItem> addFoodItem(@RequestBody FoodItem fooditem,
			@PathVariable(value = "menuid") int menuid) {
		String methodName = "addFoodItem";
		logger.info(methodName + "called");

		Menu menu = menuRepo.findById(menuid).get();
		if (menu != null) {
			fooditem.setMenu(menu);
		}
		FoodItem foodSave = foodItemService.addFoodItem(fooditem);
		return new ResponseEntity<FoodItem>(foodSave, HttpStatus.CREATED);

	}

	@GetMapping("/fooditem/{foodid}")
	public ResponseEntity<FoodItem> getFoodItem(@PathVariable("foodid") int foodid) {
		String methodName = "getFoodItem";
		logger.info(methodName + "called");

		FoodItem food = foodItemService.getFoodItem(foodid).get();

		return new ResponseEntity<FoodItem>(food, HttpStatus.OK);
	}

	@PutMapping("/fooditem/{foodid}")
	public ResponseEntity<FoodItem> updateFoodItem(@RequestBody FoodItem fooditem, @PathVariable("foodid") int foodid) {
		String methodName = "updateFoodItem";
		logger.info(methodName + "called");

		FoodItem foodItem1 = foodItemService.getFoodItem(foodid).get();
		if (foodItem1 != null) {
			foodItem1.setFoodName(fooditem.getFoodName());
			foodItem1.setPrice(fooditem.getPrice());
			foodItem1.setQuantity(fooditem.getQuantity());

		}
		foodItemService.updateFoodItem(foodItem1);

		return new ResponseEntity<FoodItem>(foodItem1, HttpStatus.CREATED);
	}

	@DeleteMapping("/fooditem/{foodid}")
	public ResponseEntity<Void> deleteFoodItem(@PathVariable("foodid") int foodid) {
		String methodName = "deleteFoodItem";
		logger.info(methodName + "called");
		foodItemService.deleteFoodItem(foodid);
		logger.info("customer deleted");

		return new ResponseEntity<Void>(HttpStatus.ACCEPTED);

	}
}
