package com.cateringmanagement.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cateringmanagement.model.Menu;
import com.cateringmanagement.repo.MenuRepo;

@Component
public class MenuService {
	@Autowired
	MenuRepo menuRepo;

	public Menu addMenu(Menu menu) {
		
		return menuRepo.save(menu);
	}

	public Optional<Menu> getMenu(int id) {
		
		return menuRepo.findById(id);
	}

	public Menu updateMenu(Menu menu) {
		
		return menuRepo.save(menu);
	}

	public void deleteMenu(int id) {
	  
		menuRepo.deleteById(id);
	}

	public Menu updateMenuById(Menu menu1) {
		
		return menuRepo.save(menu1);
	}

}
