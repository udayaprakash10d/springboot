package com.cateringmanagement.service;

import java.util.ArrayList;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.cateringmanagement.model.Customer;
import com.cateringmanagement.repo.CustomerRepo;

@Service
public class JwtUserDetailsService implements UserDetailsService {
	@Autowired
	CustomerRepo customerRepo;
	
	@Autowired
	private PasswordEncoder bcryptEncoder;

	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		Customer customer = customerRepo.findByUserName(userName);
		if (customer == null) {
			throw new UsernameNotFoundException("User not found with username: " + userName);
		}
		return new org.springframework.security.core.userdetails.User(customer.getUserName(), customer.getPassword(),
				new ArrayList<>());

	}
	
	public Customer save( Customer customer) {
	//	DAOUser newUser = new DAOUser();
		customer.setUserName(customer.getUserName());
		customer.setPassword(bcryptEncoder.encode(customer.getPassword()));
		return customerRepo.save(customer);
	}

}
