package com.cateringmanagement.service;

import java.util.Optional;
import java.util.function.Supplier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.cateringmanagement.model.Customer;
import com.cateringmanagement.repo.CustomerRepo;

@Component
public class CustomerService {
	@Autowired
	CustomerRepo customerRepo;

	public Customer getCustomer(int id) {

		return customerRepo.findById(id).get();
	}

	public Customer addCustomer(Customer customer) {

		return customerRepo.save(customer);
	}

	public void deleteCustomer(int id) {

		customerRepo.deleteById(id);
	}

	public Customer updateCustomer(Customer customer1) {

		return customerRepo.save(customer1);
	}

}
