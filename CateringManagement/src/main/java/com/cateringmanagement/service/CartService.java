package com.cateringmanagement.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cateringmanagement.model.Cart;
import com.cateringmanagement.repo.CartRepo;

@Component
public class CartService {
	@Autowired
	CartRepo cartRepo;

	public Cart addCart(Cart cart) {

		return cartRepo.save(cart);
	}

	public Cart getCartById(int cartid) {

		return cartRepo.findById(cartid).get();
	}

	public void deleteCartById(int cartid) {
		cartRepo.deleteById(cartid);

	}

	public Cart updateCart(Cart cart1) {

		return cartRepo.save(cart1);
	}

}
