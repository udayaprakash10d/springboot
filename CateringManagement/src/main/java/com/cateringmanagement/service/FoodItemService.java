package com.cateringmanagement.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cateringmanagement.model.FoodItem;
import com.cateringmanagement.repo.FoodItemRepo;

@Component
public class FoodItemService {
	@Autowired
	FoodItemRepo foodItemRepo;

	public Optional<FoodItem> getFoodItem(int foodid) {

		return foodItemRepo.findById(foodid);
	}

	public FoodItem updateFoodItem(FoodItem foodItem1) {

		return foodItemRepo.save(foodItem1);
	}

	public void deleteFoodItem(int foodid) {

		foodItemRepo.deleteById(foodid);
	}

	public FoodItem addFoodItem(FoodItem fooditem) {

		return foodItemRepo.save(fooditem);
	}

}
