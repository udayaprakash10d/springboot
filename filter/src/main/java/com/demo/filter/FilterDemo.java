package com.demo.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.springframework.stereotype.Component;
@Component
public class FilterDemo implements Filter{

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
	System.out.println("before sending request to controller");
	System.out.println("remote host"+request.getRemoteHost());
	System.out.println("remote host"+request.getRemoteAddr());
	chain.doFilter(request, response);
	System.out.println("before sending responce to client");
	}

	
}
