package com.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
	Logger log=LoggerFactory.getLogger(Controller.class);
	@GetMapping("/welcome")
	public String welcomePage()
	
	{
		log.info("hi");
		System.out.println("inside welcome mathod of controller");
		return "welcome,friends";
		
	}

}
