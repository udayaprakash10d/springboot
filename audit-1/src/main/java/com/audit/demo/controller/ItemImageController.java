package com.audit.demo.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.audit.demo.model.Item;
import com.audit.demo.model.Report;
import com.audit.demo.model.ItemImage;
import com.audit.demo.repo.AuditItemRepo;
import com.audit.demo.repo.AuditReportRepo;
import com.audit.demo.service.ItemImageService;

@RestController
public class ItemImageController {
	@Autowired
	ItemImageService itemImageService;
	@Autowired
	AuditReportRepo auditReportRepo;
	@Autowired
	AuditItemRepo auditItemRepo;
	
	@PostMapping("/itemimage/{itemid}")
	public ItemImage addImage(@RequestBody ItemImage itemImage,
			@PathVariable("itemid") int itemid)
	{
	
		Item auditItem=auditItemRepo.findById(itemid).get();
		if(auditItem!=null )
		{
			itemImage.setAuditItem(auditItem);
		}
		return itemImageService.addImage(itemImage);
	}
	
	@GetMapping("/itemimage/{imageid}")
	public Optional<ItemImage> getItemImageById(@PathVariable ("imageid") int imageid)
	{
		return itemImageService.getItemImageById(imageid);
	}
	
	@DeleteMapping("/itemimage/{imageid}")
	public void deleteItemImageById(@PathVariable("imageid") int imageid) {
		itemImageService.deleteItemImageById(imageid);
	}
	
	@PutMapping("/itemimage/{imageid}")
	public ItemImage updateItemImage(@RequestBody ItemImage itemimage, @PathVariable("imageid") int imageid) {
		ItemImage itemImage1= itemImageService.getItemImageById(imageid).get();
	
		if (itemImage1 != null) {
			itemImage1.setImageName(itemimage.getImageName());
			itemImage1.setImageUrl(itemimage.getImageUrl());
		
		
		}
		return itemImageService.updateItemImage(itemImage1);
}
	

}
