package com.audit.demo.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.audit.demo.model.Item;
import com.audit.demo.model.Comments;

import com.audit.demo.repo.AuditItemRepo;
import com.audit.demo.service.CommentsService;

@RestController
public class CommentsController {
	@Autowired
	CommentsService commentsService;
	@Autowired
	AuditItemRepo auditItemRepo;
	
	@PostMapping("/comments/{itemid}")
	public Comments addComments(@RequestBody Comments comments,
			@PathVariable("itemid") int itemid)
	{
	
		Item auditItem=auditItemRepo.findById(itemid).get();
		if(auditItem!=null )
		{
			comments.setAuditItem(auditItem);
		}
		return commentsService.addComments(comments);
	}
	@GetMapping("/comments/{commentsid}")
	public Optional<Comments> getComments(@PathVariable ("commentsid") int commentsid)
	{
		return commentsService.getComments(commentsid);
	}
	@DeleteMapping("/comments/{commentsid}")
	public void deleteCommentsById(@PathVariable("commentsid") int commentsid) {
		commentsService.deleteCommentsById(commentsid);
	}
	@PutMapping("/comments/{commentsid}")
	public Comments updateComments(@RequestBody Comments comments, @PathVariable("commentsid") int commentsid) {
		Comments comments1= commentsService.getComments(commentsid).get();
	
		if (comments1 != null) {
			comments1.setComments(comments.getComments());
	}
		return commentsService.updateComments(comments1);
}
}
