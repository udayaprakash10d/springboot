package com.audit.demo.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.audit.demo.model.Item;
import com.audit.demo.model.Report;
import com.audit.demo.repo.AuditReportRepo;
import com.audit.demo.service.ItemService;

@RestController
public class ItemController {
	@Autowired
	ItemService auditItemService;
	@Autowired
	AuditReportRepo auditReportRepo;

	@PostMapping("/audititem/{reportid}")
		public Item addItem(@RequestBody Item auditItem, @PathVariable(value = "reportid") int reportid) {
			Report auditReport=auditReportRepo.findById(reportid).get();
			if (auditReport != null) {
			auditItem.setAuditReport(auditReport);
			}
			return auditItemService.addItem(auditItem);
		}
	@GetMapping("/audititem/{itemid}")
	public Optional<Item> getItemById(@PathVariable ("itemid") int itemid)
	{
		return auditItemService.getItemById(itemid);
	}
	@DeleteMapping("/audititem/{itemid}")
	public void deleteItemById(@PathVariable("itemid") int itemid) {
		auditItemService.deleteItemById(itemid);
	}
	
	@PutMapping("/audititem/{itemid}")
	public Item updateItem(@RequestBody Item auditItem, @PathVariable("itemid") int itemid) {
		Item auditItem1= auditItemService.getItemById(itemid).get();
		System.out.println(auditItem1);
		if (auditItem1 != null) {
			auditItem1.setName(auditItem.getName());
			auditItem1.setStatus(auditItem.getStatus());
		
		}
		return auditItemService.updateItem(auditItem1);
}
}
