package com.audit.demo.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.audit.demo.model.Report;
import com.audit.demo.service.ReportService;



@RestController
public class ReportController {
	@Autowired
	ReportService auditReportservice;
	
	@PostMapping("/auditreport")
	public Report addReport(@RequestBody Report auditReport)
	{
		return auditReportservice.addReport(auditReport);
	}
	
	@GetMapping("/auditreport/{id}")
	public Optional<Report> getReportById(@PathVariable ("id") int id)
	{
		return auditReportservice.getReportById(id);
	}
	@PutMapping("/auditreport/{id}")
	public Report updateReport(@RequestBody Report auditReport, @PathVariable("id") int id) {
		Report auditReort1= auditReportservice.getReportById(id).get();
		System.out.println(auditReort1);
		if (auditReort1 != null) {
			auditReort1.setName(auditReport.getName());
			auditReort1.setStatus(auditReport.getStatus());
			auditReort1.setVersion(auditReport.getVersion());
		}
		return auditReportservice.updateReport(auditReort1);

}
	@DeleteMapping("/auditreport/{id}")
	public void deleteReport(@PathVariable("id") int id) {
		auditReportservice.deleteReport(id);
	}

}
