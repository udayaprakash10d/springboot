package com.audit.demo.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.audit.demo.model.Report;
import com.audit.demo.repo.AuditReportRepo;



@Component
public class ReportService {
	@Autowired
	AuditReportRepo auditReportRepo;

	public Report addReport(Report auditReport) {
	
		return auditReportRepo.save(auditReport);
	}

	public Optional<Report> getReportById(int id) {
		
		return auditReportRepo.findById(id);
	}

	public Report updateReport(Report auditReort1) {
		
		return auditReportRepo.save(auditReort1);
	}

	public void deleteReport(int id) {
	
		auditReportRepo.deleteById(id);
	}

}
