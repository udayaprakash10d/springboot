package com.audit.demo.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.audit.demo.model.Item;
import com.audit.demo.model.Report;
import com.audit.demo.repo.AuditItemRepo;

@Component
public class ItemService {
	@Autowired
	AuditItemRepo auditItemRepo;

	public Item addItem(Item auditItem) {
		
		return auditItemRepo.save(auditItem);
	}

	public Optional<Item> getItemById(int itemid) {

		return auditItemRepo.findById(itemid);
	}

	public void deleteItemById(int itemid) {
		 auditItemRepo.deleteById(itemid);
		
	}

	public Item updateItem(Item auditItem1) {
	
		return auditItemRepo.save(auditItem1);
	}

}
