package com.audit.demo.service;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.audit.demo.model.Comments;
import com.audit.demo.model.ItemImage;
import com.audit.demo.repo.CommentsRepo;

@Component
public class CommentsService {
	@Autowired
	CommentsRepo commentsRepo;

	public Comments addComments(Comments comments) {

		return commentsRepo.save(comments);
	}

	public Optional<Comments> getComments(int commentsid) {
		
		return commentsRepo.findById(commentsid);
	}

	public void deleteCommentsById(int commentsid) {
		commentsRepo.deleteById(commentsid);
		
	}

	public Comments updateComments(Comments comments1) {
		return commentsRepo.save(comments1);
	}

	
}
