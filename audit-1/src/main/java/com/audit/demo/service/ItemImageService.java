package com.audit.demo.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.audit.demo.model.ItemImage;
import com.audit.demo.repo.ItemImageRepo;

@Component
public class ItemImageService {
	@Autowired
	ItemImageRepo itemImageRepo;

	public ItemImage addImage(ItemImage itemImage) {
	
		return itemImageRepo.save(itemImage);
	}

	public Optional<ItemImage> getItemImageById(int imageid) {
		
		return itemImageRepo.findById(imageid);
	}

	public void deleteItemImageById(int imageid) {
		itemImageRepo.deleteById(imageid);
		
	}

	public ItemImage updateItemImage(ItemImage itemImage1) {
		
		return itemImageRepo.save(itemImage1);
	}

}
