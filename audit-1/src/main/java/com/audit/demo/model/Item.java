package com.audit.demo.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Item {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int itemId;
	public String name;
	public boolean status;
	

	@ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@JoinColumn(name = "reportId",referencedColumnName = "reportId")
	@JsonBackReference
	//@JsonIgnoreProperties(value = "auditItem")
	private Report auditReport;
	
	
	@OneToMany(targetEntity = ItemImage.class,cascade = CascadeType.ALL)
	@JoinColumn(name = "itemId")
	public List<ItemImage> itemImage;
	
	@OneToMany(targetEntity = Comments.class)
	@JoinColumn(name = "itemId")
	public List<Comments> comments;
	
	public Report getAuditReport() {
		return auditReport;
	}
	public void setAuditReport(Report auditReport) {
		this.auditReport = auditReport;
	}
	public List<ItemImage> getItemImage() {
		return itemImage;
	}
	public void setItemImage(List<ItemImage> itemImage) {
		this.itemImage = itemImage;
	}
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean getStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "AuditItem [itemId=" + itemId + ", name=" + name + ", status=" + status + "]";
	}
	
}
