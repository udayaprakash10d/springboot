package com.audit.demo.model;


import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;




@Entity
public class Report {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int reportId;
	public String name;
	public float version; 
	public boolean status;
	
	@OneToMany(targetEntity = Item.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "reportId")
	//@JsonManagedReference
	//@JsonIgnoreProperties(value = "auditReport")
	public List<Item> auditItem;
	
	public List<Item> getAuditItem() {
		return auditItem;
	}
	public void setAuditItem(List<Item> auditItem) {
		this.auditItem = auditItem;
	}
	public int getReportId() {
		return reportId;
	}
	public void setReportId(int reportId) {
		this.reportId = reportId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getVersion() {
		return version;
	}
	public void setVersion(float version) {
		this.version = version;
	}
     public boolean getStatus()
     {
    	 return status;
     }
	
	public void setStatus(boolean status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "AuditReport [reportId=" + reportId + ", name=" + name + ", version=" + version + ", status=" + status
				+ "]";
	} 
}
