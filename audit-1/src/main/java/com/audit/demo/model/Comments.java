package com.audit.demo.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class Comments {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int commentsId;
	public String  comments;
	
	@ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@JoinColumn(name = "itemId",referencedColumnName = "itemId")
	@JsonBackReference
	private Item auditItem;
	
	
	public Item getAuditItem() {
		return auditItem;
	}
	public void setAuditItem(Item auditItem) {
		this.auditItem = auditItem;
	}
	public int getCommentsId() {
		return commentsId;
	}
	public void setCommentsId(int commentsId) {
		this.commentsId = commentsId;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	@Override
	public String toString() {
		return "Comments [commentsId=" + commentsId + ", comments=" + comments + ", auditItem=" + auditItem + "]";
	}
	
	

}
