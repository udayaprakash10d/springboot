package com.audit.demo.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;


@Entity
public class ItemImage {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int imageId;
	public String imageUrl;
	public String imageName;
	
	@ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@JoinColumn(name = "itemId",referencedColumnName = "itemId")
	@JsonBackReference
	private Item auditItem;
	
	
	public Item getAuditItem() {
		return auditItem;
	}
	public void setAuditItem(Item auditItem) {
		this.auditItem = auditItem;
	}
	public int getImageId() {
		return imageId;
	}
	public void setImageId(int imageId) {
		this.imageId = imageId;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	

	@Override
	public String toString() {
		return "ItemImage [imageId=" + imageId + ", imageUrl=" + imageUrl + ", imageName=" + imageName + "]";
	}
	

}
