package com.audit.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Audit1Application {

	public static void main(String[] args) {
		SpringApplication.run(Audit1Application.class, args);
	}

}
