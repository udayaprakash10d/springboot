package com.audit.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.audit.demo.model.Report;



@Repository
public interface AuditReportRepo extends JpaRepository<Report, Integer>{

}