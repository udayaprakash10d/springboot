package com.audit.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.audit.demo.model.ItemImage;

@Repository
public interface ItemImageRepo extends JpaRepository<ItemImage, Integer>{

}
