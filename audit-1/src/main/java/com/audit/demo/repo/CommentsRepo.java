package com.audit.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.audit.demo.model.Comments;

@Repository
public interface CommentsRepo extends JpaRepository<Comments, Integer> {

}
