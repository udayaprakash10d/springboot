package com.audit.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.audit.demo.model.Item;

@Repository
public interface AuditItemRepo extends JpaRepository<Item, Integer> {

}
