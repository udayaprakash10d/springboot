package com.Learningboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.Learningboot.model.Book;

@Repository
public interface Bookrepository extends JpaRepository<Book, Integer> {

	

}
