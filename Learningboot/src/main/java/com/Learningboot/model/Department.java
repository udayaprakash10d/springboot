package com.Learningboot.model;

//import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
//import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Department {

	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	public int Did;
    
	public String Dname;
	
	public String getDname() {
		return Dname;
	}
	public void setDname(String Dname) {
		this.Dname = Dname;
	}
	@OneToOne(mappedBy = "department")
	public int getDid() {
		return Did;
	}
	public void setDid(int Did) {
		this.Did = Did;
	}
	
	
//	public Book book;
//	public Book getBook() {
//		return book;
//	}
//	public void setBook(Book book) {
//		this.book = book;
//	}
}
