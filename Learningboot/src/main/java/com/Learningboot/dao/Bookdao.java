package com.Learningboot.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.Learningboot.model.Book;
import com.Learningboot.model.Department;
import com.Learningboot.repository.Bookrepository;


@Component
public class Bookdao {
     @Autowired
	private Bookrepository bookrepository;
    // List<Book> listofbook = new ArrayList<Book>();
     
     
     
	public Book saveBook(Book book) {
		//lostofbook.add(book);

	   
		
	return	bookrepository.save(book);
		}

	public List<Book> getBook() {
	
		return bookrepository.findAll();
	}

	public Book getbookById(int bookid) {
		
		return bookrepository.findById(bookid).get();
	}

	public void delete(int bookid) {
		bookrepository.deleteById(bookid);
		
		
	}

	public void update(Book book) {
		bookrepository.save(book);
		
	}





	
     
     
}
