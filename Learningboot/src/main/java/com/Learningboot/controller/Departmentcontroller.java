package com.Learningboot.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.Learningboot.model.Department;
import com.Learningboot.service.Departmentservice;

@RestController
public class Departmentcontroller {
	
	@Autowired
	Departmentservice departmentservice;
	
	@PostMapping("/department")
	public Department savedepartment(@RequestBody Department department)
	{
		return departmentservice.savedepartment(department);
	}
	@GetMapping("/dept")
	public List<Department> get()
	{
		return departmentservice.get();
	}
	@GetMapping("/dept/{Did}")
	public Optional<Department> getbyid(@PathVariable ("Did") int Did)
	{
		return departmentservice.getbyid(Did);
	}
	


//     @RequestMapping("/dept/{dname}")
//     public List<Department> getbydname(@PathVariable ("dname")String dname)
//     {
//    	 return departmentservice.getbydname(dname);
//     }
	
	
}
