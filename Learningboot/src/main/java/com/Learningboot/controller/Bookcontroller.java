package com.Learningboot.controller;

//import java.util.List;
//import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

//import com.Learningboot.Dto.Bookdto;
import com.Learningboot.model.Book;
import com.Learningboot.service.Bookservice;

import antlr.collections.List;



@RestController
public class Bookcontroller {

	@Autowired
	Bookservice bookservice;

	@PostMapping("/book")
	public Book saveBook(@RequestBody Book book) {
		
		
		System.out.println("postmapping");
		return bookservice.saveBook(book);
	}

	//@org.springframework.web.bind.annotation.GetMapping("/book")
	@GetMapping("/book")
	public java.util.List<Book> getAllbook() {
		
		
		return bookservice.getAllBook();
}   
	
	@GetMapping("/book/{bookid}")
      public Book getbook(@PathVariable("bookid")int bookid)
     {
    	 return bookservice.getbookById(bookid);
     }
      @DeleteMapping("/book/{bookid}")
     public void deleteBook(@PathVariable("bookid")int bookid)
     {
    	 bookservice.delete(bookid);
     }
      @PutMapping("/book")
     public Book update(@RequestBody Book book) 
     {
       bookservice.update(book);
       return book;
       
     }

}
