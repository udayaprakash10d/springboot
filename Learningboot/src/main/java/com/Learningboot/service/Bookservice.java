package com.Learningboot.service;

//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.Learningboot.dao.Bookdao;
import com.Learningboot.model.Book;

@Component
public class Bookservice {

	
		@Autowired
		
		Bookdao bookdao;

		public Book saveBook(Book book) {
			return bookdao.saveBook(book);
		}

		public List<Book> getAllBook() {
			return bookdao.getBook();
		}

		public  Book getbookById(int bookid) {
			
		   
			return bookdao.getbookById(bookid);
		}

		public void delete(int bookid) {
			bookdao.delete(bookid);
			
		}

		public void update(Book book) {
			bookdao.update(book);
			
		}

	

	

	

		

	
	}

