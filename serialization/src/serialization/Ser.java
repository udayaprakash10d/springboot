package serialization;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Ser {
	public static void main(String args[]) throws Exception 
	{
		Pser s1=new Pser(100, "udaya");
		FileOutputStream fout=new FileOutputStream("ser01.txt");   //creat file
		ObjectOutputStream out=new ObjectOutputStream(fout);
		out.writeObject(s1);                       ///serialization
		System.out.println(s1);
		out.flush();
		
		
		
		ObjectInputStream in=new ObjectInputStream(new FileInputStream("ser01.txt"));
	    Pser s2=(Pser)in.readObject();                                                 //deserialization
		System.out.println(s2.id+" "+s2.name);
		in.close();
	}

}
