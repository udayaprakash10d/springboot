package com.interceptor;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
	@GetMapping("/user")
	public List<String> getUser()
	{
		List<String> user =new ArrayList<>();
		user.add("udaya");
		user.add("naveen");
		user.add("anto");
		return user;
	}

}
