package com.interceptor;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductController {
	@GetMapping("/product")
	public List<String> getProduct(){
		List<String> product =new ArrayList<>();
		product.add("computer");
		product.add("airconditioner");
		product.add("television");
		return product;
		
	}

}
