package com.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

public class GeneralInterseptor implements HandlerInterceptor  {
      Logger log=LoggerFactory.getLogger(GeneralInterseptor.class);
      
      @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
    		throws Exception {
    	// TODO Auto-generated method stub
    	  log.info("prehandle in general interseptor");
    	return HandlerInterceptor.super.preHandle(request, response, handler);
    }
      
        @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
    		ModelAndView modelAndView) throws Exception {
    	// TODO Auto-generated method stub
    	  log.info("posthandle in general interseptor");
    	HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }
      
      @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
    		throws Exception {
    	// TODO Auto-generated method stub
    	  log.info("aftercompletion in general interseptor");
    	HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }


}
