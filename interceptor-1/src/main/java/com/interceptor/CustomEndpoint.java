package com.interceptor;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.stereotype.Component;
@Component
@Endpoint(id = "customendpoint")
public class CustomEndpoint {
	
	public Map<String, Object> refrerence=new HashMap<>();
	@ReadOperation
	public Map<String, Object> customEndpoint(){
		refrerence.put("custom endpoint", "example:custom endpoint for actuator");
		return refrerence;
	}

}
