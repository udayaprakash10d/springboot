package com.junit.demo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class Testcase {
	@BeforeClass
	 public static void preClass() {
		 System.out.println("This is the preClass() method that runs one time before the class");
		     }
	@Before
	public void setup()
	{
		System.out.println("before test setup method called");
		
	}
	@After
	public void abc()
	{
		System.out.println("after test abc method called");
	}
	@Test
	public void testAdd()
	{
		assertEquals(6,Code.add(3,3));
		assertNotEquals(10,Code.add(3,3));
	}
	
	String str = "Junit is working fine";
	@Test
	public void stringTest()
	{
	 assertEquals("Junit is working fine", str);
	 assertNotEquals("abc",str);
}
	
	 @Test
	    public void testConcatenate() {
	    assertEquals("onetwo", Code.concatenate("one", "two"));
            
	    }
	 @Test
	 public void testFindMax(){  
			assertEquals(4,Code.findMax(new int[]{1,3,4,2}));  
			assertEquals(-1,Code.findMax(new int[]{-12,-1,-3,-4,-2}));  
		}  

	 @AfterClass
	 public static void postClass() {
		 System.out.println("This is the postClass() method that runs one time after the class");
		     }
}