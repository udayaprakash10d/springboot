package com.customannotation;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;




@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@interface Smartphone
{
	String os() default "windows";
	int version() default 1;
}



@Smartphone(os="android",version=7)
class NokiaASeries
{
	
	String model;
	int size;
	public NokiaASeries(String model, int size) {
		
		this.model = model;
		this.size = size;
	}
	
	
}
public class Annotationdemo {
	public static void main(String args[])
	{
		NokiaASeries obj= new NokiaASeries("c100",5);
		
		
		Class c=obj.getClass();
		Annotation an=c.getAnnotation(Smartphone.class);
		Smartphone s=(Smartphone)an;
		System.out.println(s.os());
	}

}
